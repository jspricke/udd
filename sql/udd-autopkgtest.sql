--
-- Name: autopkgtestLE; Schema: public; Owner: -
--

CREATE TABLE public.autopkgtest (
    run_id int NOT NULL,
    source text NOT NULL,	-- in autopkgtest metadate named 'package'
    suite text NOT NULL,	-- unstable, testing, stable  thus not really matching "release"
    architecture text NOT NULL,	-- in autopkgtest metadate named 'arch'
    version text,
    trigger text,		-- usually package.*version
    status text NOT NULL,
    requestor text,		-- 'britney', 'debci' or e-mail
    -- pin_packages text, -- ?? always empty [] leave it out for the moment
    autopkgtest_date timestamp without time zone NOT NULL,
    duration_seconds int,
    last_pass_date timestamp without time zone,
    last_pass_version text,
    message text,
    previous_status text,
    PRIMARY KEY (run_id),
    UNIQUE (source, suite, architecture),
    CONSTRAINT check_suite CHECK ((suite = ANY (ARRAY['unstable'::text, 'testing'::text, 'stable'::text]))),
    CONSTRAINT check_arch CHECK ((architecture = ANY (ARRAY['amd64'::text, 'arm64'::text, 'ppc64el'::text])))
);


GRANT SELECT ON TABLE public.autopkgtest TO PUBLIC;
GRANT ALL ON TABLE public.autopkgtest TO udd;
