require 'logger'
require 'cron_format'
require 'cgi'

def dbget(dbh, q, *args)
  rows, sth = nil
  sth = dbh.prepare(q)
  sth.execute(*args)
  rows = sth.fetch_all
  return rows
end

def update_status(imps)
  now = Time::now
  db = PG.connect({ :dbname => 'udd', :user => 'guest', :host => 'localhost', :port => 5452})
  res = db.exec("select importer, max(time) as last_time from udd_logs group by importer")
  last_time = Hash[res.values]
  res = db.exec("select importer, max(time) as last_ok from udd_logs where status = 0 group by importer")
  last_ok = Hash[res.values]

  db.close

  imps.each_pair do |imp, v|
    v.delete('late') # cleanup

    # compute interval
    nextrun = CronFormat.new(v['cron'], Time::now).to_time
    nextrun2 = CronFormat.new(v['cron'], nextrun+60).to_time
    v['interval'] = nextrun2 - nextrun

    if last_time.has_key?(imp)
      v['last_time'] = Time::parse(last_time[imp])
      v['next_time'] = CronFormat.new(v['cron'], v['last_time']).to_time
      if v['next_time'] <= v['last_time']
        v['next_time'] = CronFormat.new(v['cron'], v['last_time'] + 61).to_time # hack
      end
      if now > v['next_time']
        v['late'] = now - v['next_time']
      end
    else
      imps[imp]['last_time'] = nil
      imps[imp]['next_time'] = nil
    end
    imps[imp]['last_ok'] = last_ok[imp]
  end
end

def load_importers
  importers = JSON::load(IO::read(udd_dir + 'rudd.conf'))['importers']
  importers.each_pair do |imp, v|
    v['name'] = imp
    v['pool'] = 'default' if not v['pool']
  end
end

def init_log
  file = open(udd_dir + 'rudd.log', File::WRONLY | File::APPEND | File::CREAT)
  file.sync = true
  STDOUT.reopen(file)
  STDERR.reopen(file)
  $log = Logger.new(file)
  $log.level = Logger::DEBUG
end

def check_load(log,pool)
  while true do
    load = IO::read('/proc/loadavg').split[1].to_f
    sleeptime = 600
    # limit in cgi's is typically 20, let's give the importers a higher limit,
    # so that web requests are stopped before importers are stopped
    if load > 30
      log.info "load #{load}, not running next job in pool #{pool}, sleep #{sleeptime}"
      sleep(sleeptime)
    else
      log.debug "load #{load}, running next job in pool #{pool}"
      return
    end
  end
end

def run_bugs_loop
  init_log if not defined?($log)
  while true do
    check_load($log,"bugs")
    $log.debug "Running bugs"
    now = Time::now
    log = `DEBBUGS_CONFIG_FILE=/srv/udd.debian.org/udd/bugs-config perl /srv/udd.debian.org/udd/udd/bugs_gatherer.pl /srv/udd.debian.org/udd/config-ullmann.yaml daemon-run foo 2>&1`
    status = $?.exitstatus
    duration = (Time::now - now).to_i
    $log.debug "Finished running bugs (duration: #{duration}; status: #{status})"
    log.encode!('UTF-8', :undef => :replace, :invalid => :replace, :replace => "")
    log = log.split(/\n/)
    if status == 0 # clean log
      log.reject! { |l| l =~ /: unable to open \/srv\/bugs.debian.org\/versions\/pkg\// }
      log.reject! { |l| l =~ /Use of uninitialized value in hash slice at / }
      log.reject! { |l| l =~ /Unmatched \(\) / }
      log.reject! { |l| l =~ /Unmatched '<>' in "/ }
      log.reject! { |l| l =~ /Use of uninitialized value in subroutine entry at/ }
      log.reject! { |l| l =~ /Mail::Address::_tokenise/ }
      log.reject! { |l| l =~ /Mail::Address::parse/ }
      log.reject! { |l| l =~ /main::run\('HASH/ }
      log.reject! { |l| l =~ /main::update_bugs\('HASH/ }
      log.reject! { |l| l =~ /main::update_bug\('HASH/ }
      log.reject! { |l| l =~ /main::main\(\) called at \/srv\/udd.debian.org\/udd\/udd\/bugs_gatherer.pl line/ }
      log.reject! { |l| l =~ /Wide character in print at \/srv\/bugs.debian.org\/perl\/Debbugs\/Packages.pm line/ }
      log.reject! { |l| l =~ /does not map to Unicode at/ }
    end
    rerun = false
    if not log.select { |l| l =~ /^Not importing .* bugs due to limit/ }.empty?
      unarchline = log.select { |l| l =~ /^Not importing .* unarchived bugs/ }
      if not unarchline.empty?
        unarch = unarchline.first.split(' ')[2]
      else
        unarch = 0
      end
      archline = log.select { |l| l =~ /^Not importing .* archived bugs/ }
      if not archline.empty?
        arch = archline.first.split(' ')[2]
      else
        arch = 0
      end
      log.reject! { |l| l =~ /^Not importing .* bugs due to limit/ }
      rerun = true
      $log.debug "bugs re-running immediately because not all bugs were processed (missing #{unarch} unarchived, #{arch} archived)."
    else
      $log.debug "bugs processed all pending bugs -- sleeping for 60s."
    end
    log = log.join("\n")
    db = PG.connect({ :dbname => 'udd', :port => 5452})
    db.exec("insert into udd_logs values('bugs', '#{now}', '#{duration}', '#{status}', '#{db.escape_string(log)}')")
    db.close
    if not rerun
      sleep 60
    end
  end
end

def run_importer(imp)
  raise 'bugs importer is special' if imp == 'bugs'
  init_log if not defined?($log)
  $log.debug "Running #{imp['name']}"
  now = Time::now
  if imp.has_key?('command')
    cmd = imp['command']
  else
    cmd = udd_dir + "rudd --built-in #{imp['name']}"
  end
  log = `#{cmd} 2>&1`
  status = $?.exitstatus
  duration = (Time::now - now).to_i
  $log.debug "Finished running #{imp['name']} (duration: #{duration}; status: #{status})"
  db = PG.connect({ :dbname => 'udd', :port => 5452})
  db.exec("insert into udd_logs values('#{imp['name']}', '#{now}', '#{duration}', '#{status}', '#{db.escape_string(log)}')")
  db.close
  return status
end

def run_daemon
  init_log
  importers = load_importers
  importers.to_a.group_by { |imp| imp[1]['pool'] }.each_pair do |pool, imps|
    next if pool == 'none' or pool == 'bugs' # those are special
    Thread::new(pool, imps) do |pool, imps|
      Thread.current.abort_on_exception = true
      $log.debug "Starting pooler: #{pool}"
      imps = Hash[imps]
      while true do
        check_load($log,pool)
        update_status(imps)

        never_ran = imps.values.select { |imp| imp['last_time'].nil? }
        late = imps.values.select { |imp| imp.has_key?('late') }.sort { |a, b| a['late'] <=> b['late'] }.reverse

        if not never_ran.empty?
          never_ran.each do |imp|
            $log.info "[pool #{pool}] Running #{imp['name']} for the first time"
            run_importer(imp)
          end
        elsif not late.empty?
          late.each do |imp|
            $log.debug "[pool #{pool}] Running #{imp['name']} -- late by #{imp['late']}" if imp['late'] > 60
            run_importer(imp)
          end
        else
          # sleep until next run
          nextrun = imps.values.select { |imp| not imp['next_time'].nil? }.sort { |a, b| a['next_time'] <=> b['next_time'] }.first['next_time']
          $log.debug "[pool #{pool}] Nothing to do, sleeping until #{nextrun}"
          t = nextrun - Time::now
          sleep(t) if t > 0 # avoid race condition where Time::now > nextrun
        end
      end
    end
  end
  Thread::new do # bugs pool
    Thread.current.abort_on_exception = true
    $log.debug "Starting pooler: bugs"
    run_bugs_loop
  end
  Thread.list.each { |th| th.join if Thread.current != th }
end

def udd_status_html
  importers = load_importers
  update_status(importers)
puts <<-EOF
<!DOCTYPE html>
<html>
<head>
<title>UDD status page</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<script   src="https://code.jquery.com/jquery-3.0.0.min.js"   integrity="sha256-JmvOoLtYsmqlsWxa7mDSLMwa6dZ9rrIdtrrVYRnDRH0="   crossorigin="anonymous"></script>

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
  
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#importers').dataTable({"paging" : false});
    });
</script>
</head>
<body>
<div class="container-fluid">
<p><b>Current time: #{Time::now.to_s}</b></p>
<table class="table table-striped table-bordered table-condensed" id="importers">
<thead>
<tr><th>importer</th><th>pool</th><th>last success</th><th>last time</th><th>next time</th><th>interval</th><th>comment</th></tr>
</thead>
</tbody>
EOF
importers.values.sort { |a,b| (a['next_time'] || Time::at(0)) <=> (b['next_time'] || Time::at(0)) }.each do |imp|
  comment, tag = ''
  if imp['last_time'].nil?
    comment = 'never ran'
    tag = 'class="danger"'
  elsif imp.has_key?('late')
    comment = "late by #{imp['late']}s"
    if imp['late'] > 86400
      tag = 'class="danger"'
    elsif imp['late'] > 300
      tag = 'class="warning"'
    end
  end
  puts "<tr #{tag}><td>#{imp['name']}</td><td>#{imp['pool']}</td><td>#{imp['last_ok']}</td><td>#{imp['last_time'] || ''}</td><td>#{imp['next_time'] || ''}</td><td>#{imp['interval'].to_i}</td><td>#{comment}</td></tr>"
end
  puts "</tbody></table>"

  puts "<h1>importers with errors</h1>"
  db = Sequel.connect(UDD_GUEST)
  db["select * from udd_logs where (importer, time) in (select importer, max(time) from udd_logs group by importer) and status != 0"].all.sym2str.each do |r|
    puts "<h3>#{r['importer']} (last: #{r['time']}, status: #{r['status']})</h3>"
    puts "<pre>#{CGI.escapeHTML(r['log'])}</pre>"
  end

  puts "<h1>importers without errors but still non-empty output</h1>"
  db["select * from udd_logs where (importer, time) in (select importer, max(time) from udd_logs group by importer) and status = 0 and log != ''"].all.sym2str.each do |r|
    puts "<h3>#{r['importer']} (last: #{r['time']}, status: #{r['status']})</h3>"
    puts "<pre>#{CGI.escapeHTML(r['log'])}</pre>"
  end
  puts "</div></body></html>"
end

def udd_status
  importers = load_importers
  update_status(importers)
  st = false
  late = importers.values.select { |imp| imp.has_key?('late') and imp['late'] > imp['interval'] }
  if not late.empty?
    puts "Importers not running recently:"
    late.each do |imp|
      puts " - #{imp['name']} (last run: #{imp['last_time']})"
    end
    st = true
  end

  db = Sequel.connect(UDD_GUEST)
  res = db["select * from udd_logs where (importer, time) in (select importer, max(time) from udd_logs group by importer) and status != 0"].all.sym2str
  if not res.empty?
    puts "Importers exiting with errors:"
    res.each do |r|
      res2 = db["select max(time) from udd_logs where importer = '#{r['importer']}' and status = 0"].all.hash_values.first[0]
      if res2.nil?
         last_ok = 'never'
      else
         last_ok = res2.to_s
      end
      puts " - #{r['importer']} (last: #{r['time']}, status: #{r['status']}, last OK: #{last_ok})"
    end
    st = true
  end
  if st
    puts "\nDetailed status: https://udd.debian.org/udd-status.cgi"
  end
end
