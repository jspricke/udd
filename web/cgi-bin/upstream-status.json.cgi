#!/usr/bin/ruby
$:.unshift('../../rlibs')
require 'udd-db'
require 'pp'
require 'cgi'
require 'json'

STDERR.reopen(STDOUT)
puts "Content-type: text/json\n\n"

def enc(str)
  return nil if str.nil?
  str.encode('UTF-8', :undef => :replace, :invalid => :replace, :replace => "?")
end

DB = Sequel.connect(UDD_GUEST)
q = <<-EOF
select source, debian_mangled_uversion, debian_uversion, status, upstream_url, upstream_version, warnings, errors
from upstream
where status is not null
and release='sid'
EOF
rows = DB[q].all.sym2str
rows2 = rows.map { |r| {
  'debian-mangled-uversion' => enc(r['debian_mangled_uversion']),
  'debian-uversion' => enc(r['debian_uversion']),
  'package' => r['source'].to_s,
  'status' => r['status'],
  'upstream-url' => r['upstream_url'],
  'upstream-version' => r['upstream_version'],
  'warnings' => enc(r['warnings']) || nil,
  'errors' => enc(r['errors']) || nil,
}
}
puts JSON::pretty_generate(rows2)
