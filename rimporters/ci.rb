def update_ci
  db = PG.connect({ :dbname => 'udd', :port => 5452})

  db.exec('BEGIN')
  # FIXME there might be more suites at some point
  db.exec('DELETE FROM ci')
  db.prepare('ci_insert', 'INSERT INTO ci (suite, arch, source, version, date, run_id, status, blame, previous_status, duration, message) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)')
  ['unstable', 'testing', 'stable'].each do |suite|
    ['amd64', 'arm64', 'ppc64el'].each do |arch|
      packages = JSON::parse(open("https://ci.debian.net/data/status/#{suite}/#{arch}/packages.json").read)
      packages.each do |pkg|
        pkg['suite'] = suite
        pkg['arch'] = arch
        pkg['date'] = Time::parse(pkg['date']) rescue Time::parse(pkg['updated_at'])
        pkg['blame'] = pkg['blame'].inspect.to_s
        pkg['duration_seconds'] = pkg['duration_seconds'].to_i
        db.exec_prepared('ci_insert', 
                         ['suite', 'arch', 'package', 'version', 'date', 'run_id', 'status', 'blame',
                          'previous_status', 'duration_seconds', 'message'].map { |e| pkg[e] }
                        )
      end
    end
  end
  db.exec("COMMIT")
end
